const theme = {
  spacing: {
    unit: 10,
  },
  colors: {
    primary: '#00aeef',
    secondary: '#ffffff',
    third: '#fecf39',
    borderColor: '#e8e8e8',
    success: '#25b10f',
    error: '#ED1C24',
    lightGrey: 'rgba(0, 0, 0, 0.26)',
    backgroundColor: '#eff3f6',
    linkStyle: '#1890ff',
    lightBlue: '#40a9ff',
    primaryText: 'rgba(0, 0, 0, 0.85)',
    secondaryText: 'rgba(0, 0, 0, 0.65)',
  },
  borders: {
    containerBorder: '1px solid #e8e8e8',
  }
};

export default theme;
