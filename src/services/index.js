import axios from 'axios';
import API from '../constants/api';

const API_AUTH = {
  username: API.LOGIN,
  password: API.PASS,
};

const services = () => {
  const getCategories = () => axios.get(`${API.PROXY}${API.LINK}/categories`,
    { auth: API_AUTH });
  const addCategory = (body) => axios.post(`${API.PROXY}${API.LINK}/categories`, body, { auth: API_AUTH });
  const editCategory = (categoryId, body) => axios.put(`${API.PROXY}${API.LINK}/categories/${categoryId}`,
    body, { auth: API_AUTH });
  const removeCategory = (categoryId) => axios.delete(`${API.PROXY}${API.LINK}/categories/${categoryId}`,
    { auth: API_AUTH });
  return {
    getCategories,
    addCategory,
    editCategory,
    removeCategory,
  }
};

export default services();
