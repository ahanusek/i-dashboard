import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Alert } from 'antd';
import injectSheet from 'react-jss';
import { FormEventsEmitter } from 'react-components-form';

const styles = () => ({
  modalContainer: {
    '& .ant-switch-checked': {
      backgroundColor: '#25b10f',
    },
    '& .ant-select': {
      '& img': {
        height: 30,
        width: 30,
      }
    }
  }
});

const ModalFormHOC = (WrappedComponent) => {
  class ModalForm extends Component {
    state = { eventsEmitter: new FormEventsEmitter()};
    onSubmit = (data) => {
      this.props.onSubmit(data);
    };
    renderErrorAlert = () => {
      const { model, errorObject } = this.props;
      if (model && errorObject === 500) {
        return (
          <Alert
            message="Nie masz uprawnień aby edytować tę kategorię"
            description="Skontaktuj się z administratorem."
            type="error"
            closable
            showIcon
          />
        )
      }
      return (
        <Alert
          message="Wystąpił problem podczas przesyłania danych"
          description="Sprawdź dane i spróbuj jeszcze raz."
          type="error"
          closable
          showIcon
        />
      )
    }
    render() {
      const { eventsEmitter } = this.state;
      const {
        open,
        handleClose,
        model,
        title,
        errorObject,
        loading,
        saveButtonLabel,
        disabledSaveButton = false,
        width,
        classes,
        ...restProps } = this.props;
      return (
        <Modal
          width={width}
          visible={open}
          destroyOnClose
          maskClosable={false}
          wrapClassName={classes.modalContainer}
          title={title}
          onOk={() => eventsEmitter.emit('submit')}
          onCancel={handleClose}
          footer={[
            <Button
              key="back"
              onClick={handleClose}
            >
              Anuluj
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={() => eventsEmitter.emit('submit')}
              disabled={disabledSaveButton}
            >
              {saveButtonLabel || 'Zapisz'}
            </Button>,
          ]}
        >
          <WrappedComponent
            onSubmit={this.onSubmit}
            eventsEmitter={eventsEmitter}
            model={model}
            {...restProps}
          />
          { errorObject && this.renderErrorAlert() }
        </Modal>
      );
    }
  }

  ModalForm.defaultProps = {
    width: 550,
    errorObject: null,
    model: null,
    disabledSaveButton: false,
    saveButtonLabel: '',
  }

  ModalForm.propTypes = {
    title: PropTypes.string.isRequired,
    width: PropTypes.number,
    onSubmit: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    errorObject: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string, PropTypes.number]),
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    model: PropTypes.shape({}),
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    saveButtonLabel: PropTypes.string,
    disabledSaveButton: PropTypes.bool,
  };
  return injectSheet(styles)(ModalForm);
};

export default ModalFormHOC;
