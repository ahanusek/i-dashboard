import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import { FieldConnect } from 'react-components-form';

const FormItem = Form.Item;
const { TextArea } = Input;


const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};


const TextAreaField = ({
  className,
  onChange,
  name,
  validationErrors,
  hasValidationError,
  value,
  required,
  label,
  rows,
  placeholder,
  defaultValue,
  disabled,
  fieldAttributes,
}) => (
  <FormItem
    {...formItemLayout}
    label={label}
    validateStatus={hasValidationError ? 'error' : null}
    required={required}
    help={validationErrors && validationErrors[0]}
  >
    <TextArea
      onChange={(e) => onChange(e.target.value)}
      type="text"
      id={name}
      rows={rows}
      placeholder={placeholder}
      disabled={disabled}
      value={value}
      defaultValue={defaultValue}
      className={className}
      {...fieldAttributes}
    />
  </FormItem>
)


TextAreaField.defaultProps = {
  value: '',
  fieldAttributes: {},
  defaultValue: '',
  placeholder: '',
  required: false,
  hasValidationError: false,
  className: '',
  disabled: false,
  validationErrors: null,
  rows: 2,
}

TextAreaField.propTypes = {
  onChange: PropTypes.func.isRequired,
  hasValidationError: PropTypes.bool,
  validationErrors: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
    PropTypes.string,
    PropTypes.shape({})
  ]),
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  fieldAttributes: PropTypes.shape({}),
  className: PropTypes.string,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  rows: PropTypes.number,
};

export default FieldConnect(TextAreaField);
