import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FieldConnect } from 'react-components-form';
import { Form, Switch } from 'antd';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

export class SwitchField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.value || false,
      value: props.checkboxValue || true
    };
  }

  componentWillReceiveProps({ value, checkboxValue }) {
    this.setState({ checked: value, value: checkboxValue });
  }

  getValue(checked) {
    const { type } = this.props;
    const { value } = this.state;
    if (type === Boolean) return !!checked;
    return checked ? value : undefined;
  }

  toggleValue = () => {
    const { onChange } = this.props;
    const { checked } = this.state;
    onChange(this.getValue(!checked));
    this.setState({
      checked: !checked
    });
  }
  render() {
    const {
      required,
      name,
      validationErrors,
      hasValidationError,
      label,
      disabled,
      defaultValue,
      fieldAttributes,
    } = this.props;
    return (
      <FormItem
        {...formItemLayout}
        label={label}
        validateStatus={hasValidationError ? 'error' : null}
        required={required}
        help={validationErrors && validationErrors[0]}
      >
        <Switch
          onChange={this.toggleValue}
          id={name}
          defaultChecked={defaultValue}
          disabled={disabled}
          checked={this.state.checked}
          {...fieldAttributes}
        />
      </FormItem>
    );
  }
}

SwitchField.defaultProps = {
  wrapperClassName: '',
  className: '',
  fieldAttributes: {},
  validationErrors: null,
  required: false,
  disabled: false,
  defaultValue: false,
  hasValidationError: false,
  checkboxValue: false,
}

SwitchField.propTypes = {
  type: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.shape({}),
    PropTypes.func,
  ]),
  wrapperClassName: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  validationErrors: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
    PropTypes.string,
    PropTypes.shape({})
  ]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  defaultValue: PropTypes.bool,
  hasValidationError: PropTypes.bool,
  value: PropTypes.bool.isRequired,
  checkboxValue: PropTypes.bool,
  label: PropTypes.string.isRequired,
  fieldAttributes: PropTypes.shape({})
};

SwitchField.defaultProps = {
  type: Boolean
};

export default FieldConnect(SwitchField);
