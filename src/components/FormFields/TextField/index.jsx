import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import { FieldConnect } from 'react-components-form';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const TextField = ({
  defaultValue,
  className,
  onChange,
  name,
  validationErrors,
  hasValidationError,
  value,
  required,
  label,
  placeholder,
  disabled,
  addonBefore,
  validateStatus,
  errorMessage,
  addonAfter,
  fieldAttributes,
  ...restProps,
}) => (
  <FormItem
    {...formItemLayout}
    label={label}
    validateStatus={hasValidationError && 'error' || validateStatus}
    required={required}
    help={validationErrors && validationErrors[0] || errorMessage}
    {...restProps}
  >
    <Input
      onChange={(e) => onChange(e.target.value)}
      type="text"
      id={name}
      placeholder={placeholder}
      disabled={disabled}
      value={value}
      defaultValue={defaultValue}
      className={className}
      addonBefore={addonBefore}
      addonAfter={addonAfter}
      {...fieldAttributes}
    />
  </FormItem>
    );

TextField.defaultProps = {
  value: '',
  fieldAttributes: {},
  defaultValue: '',
  placeholder: '',
  required: false,
  hasValidationError: false,
  className: '',
  disabled: false,
  addonBefore: null,
  addonAfter: null,
  validateStatus: '',
  errorMessage: '',
  validationErrors: null,
}

TextField.propTypes = {
  onChange: PropTypes.func.isRequired,
  hasValidationError: PropTypes.bool,
  validationErrors: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
    PropTypes.string,
    PropTypes.shape({})
  ]),
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  fieldAttributes: PropTypes.shape({}),
  className: PropTypes.string,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  addonBefore: PropTypes.node,
  addonAfter: PropTypes.node,
  validateStatus: PropTypes.string,
  errorMessage: PropTypes.string,
};

export default FieldConnect(TextField);
