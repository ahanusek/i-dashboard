import React from 'react';
import { shallow } from 'enzyme';
import { CategoriesList } from '../';
import CategoryItem from './';

describe('CategoryItem', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = {
      removeCategory: jest.fn(),
      editCategory: jest.fn(),
      addSubCategory: jest.fn(),
      category: {},
    }
    wrapper = shallow(<CategoryItem {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should show Alert if category.children list is empty', () => {
    expect(wrapper.find('Alert')).toHaveLength(1);
  });
  it('should render CategoriesList if category.children has values', () => {
    expect(wrapper.find(CategoriesList)).toHaveLength(0);
    wrapper.setProps({
      category: {
        children: [
          { name: 'test', id: 4 },
        ],
      },
    });
    expect(wrapper.find(CategoriesList)).toHaveLength(1);
  });
});
