import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Alert } from 'antd';
import { CategoriesList } from "../index";

const CategoryItem = ({ category, removeCategory, editCategory, addSubCategory }) => (
  <div>
    {isEmpty(category.children) ?
      <div>
        <Alert message="Brak dodanych podkategorii" type="info" />
      </div>
      :
      <CategoriesList
        categories={category.children}
        removeCategory={removeCategory}
        editCategory={editCategory}
        addSubCategory={addSubCategory}
      />
    }
  </div>
)

CategoryItem.propTypes = {
  category: PropTypes.shape({
    children: PropTypes.array,
  }).isRequired,
  removeCategory: PropTypes.func.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired,
}

export default CategoryItem;
