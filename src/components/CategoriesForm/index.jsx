import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip, Icon, Row, Col } from 'antd';
import { Form } from 'react-components-form';
import { TextField, SwitchField, TextAreaField } from "../FormFields/index";
import categoriesSchema from '../../helpers/formSchemas'
import { ModalFormHOC } from "../index";

const CategoriesForm = ({ eventsEmitter, onSubmit, model, ...props}) => (
  <Form
    onSubmit={onSubmit}
    eventsEmitter={eventsEmitter}
    schema={categoriesSchema}
    model={model && { ...model }}
    {...props}
  >
    <Row gutter={8}>
      <Col xs={24}>
        <TextField
          name="name"
          label="Nazwa kategorii"
        />
        <SwitchField
          name="is_visible"
          label={(
            <span>
              Status&nbsp;
              <Tooltip title="Czy kategoria ma być widoczna dla pozostałych użytkowników">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
        />
        <TextAreaField
          name="description"
          label="Opis kategorii"
          rows={3}
        />
      </Col>
    </Row>
  </Form>
);

CategoriesForm.defaultProps = {
  model: null,
  eventsEmitter: null,
};

CategoriesForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  eventsEmitter: PropTypes.shape({
    emit: PropTypes.func,
  }),
  model: PropTypes.shape({
    name: PropTypes.string,
    is_visible: PropTypes.bool,
    description: PropTypes.string,
  }),
};

export default ModalFormHOC(CategoriesForm);
