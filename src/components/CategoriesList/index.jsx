import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Collapse } from 'antd';
import injectSheet from 'react-jss';
import isEmpty from 'lodash/isEmpty';
import { CategoryItem } from "../index";
import { PanelHeader } from "./SubComponents/index";
import styles from './styles';

const Panel = Collapse.Panel;


export const CategoriesList = ({ categories, removeCategory, editCategory, addSubCategory, classes }) => {
  if(isEmpty(categories)) {
    return (
      <div className={classes.container}>
        <Alert
          message="Brak kategorii"
          description="Skorzystaj z formularza i dodaj pierwszą kategorię"
          type="info"
          showIcon
        />
      </div>
    )
  }
  return (
    <div className={classes.container}>
      <Collapse>
        {
        categories.map(category => (
          <Panel
            header={
              <PanelHeader
                category={category}
                removeCategory={removeCategory}
                editCategory={editCategory}
                addSubCategory={addSubCategory}
              />
            }
            key={category.id}
          >
            <CategoryItem
              category={category}
              removeCategory={removeCategory}
              editCategory={editCategory}
              addSubCategory={addSubCategory}
            />
          </Panel>
          ))
      }
      </Collapse>
    </div>
  )
}

CategoriesList.defaultProps = {
  categories: [],
};

CategoriesList.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object),
  removeCategory: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

export default injectSheet(styles)(CategoriesList);
