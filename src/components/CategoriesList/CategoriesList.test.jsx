import React from 'react';
import { shallow } from 'enzyme';
import { CategoriesList } from './';

describe('CategoriesList', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = {
      removeCategory: jest.fn(),
      editCategory: jest.fn(),
      addSubCategory: jest.fn(),
      classes: {},
      categories: [],
    }
    wrapper = shallow(<CategoriesList {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should show Alert if categories list is empty', () => {
    expect(wrapper.find('Alert')).toHaveLength(1);
  });
  it('should render Collapse with Panels if categories exists', () => {
    expect(wrapper.find('Collapse')).toHaveLength(0);
    expect(wrapper.find('CollapsePanel')).toHaveLength(0);
    expect(wrapper.find('CategoryItem')).toHaveLength(0);
    wrapper.setProps({
      categories: [
        { name: 'test', id: 1 },
        { name: 'test', id: 2 },
      ]
    });
    expect(wrapper.find('Collapse')).toHaveLength(1);
    expect(wrapper.find('CollapsePanel')).toHaveLength(2);
    expect(wrapper.find('CategoryItem')).toHaveLength(2);
  });
});
