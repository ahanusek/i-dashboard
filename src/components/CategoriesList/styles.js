const styles = theme => ({
  container: {
    width: '100%',
  },
  panelContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  toolbar: {
    marginRight: 20,
  },
  statusContainer: {
    display: 'inline-block',
    minWidth: '140px',
  },
  status: {
    marginRight: 10,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
  },
  name: {
    marginLeft: 10,
    fontWeight: 500,
  },
  linkStyle: {
    color: theme.colors.linkStyle,
    cursor: 'pointer',
    '&:hover': {
      color: theme.colors.lightblue,
    },
  }
});

export default styles;
