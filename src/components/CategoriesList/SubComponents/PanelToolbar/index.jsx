import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Icon, Divider, Badge, Popconfirm } from 'antd';
import injectSheet from 'react-jss';
import styles from '../../styles';

export class PanelToolbar extends Component {
  stopPropagation = (e) => {
    e.stopPropagation();
  }
  handleRemove = (e) => {
    const { category, removeCategory } = this.props;
    this.stopPropagation(e)
    removeCategory(category.id)
  }
  handleEdit = (e) => {
    const { category, editCategory } = this.props;
    this.stopPropagation(e);
    editCategory(category);
  }
  addSubCategory = (e) => {
    const { category, addSubCategory } = this.props;
    this.stopPropagation(e);
    addSubCategory(category.id || 1);
  }
  render() {
    const { category, classes } = this.props;
    return (
      <div className={classes.toolbar}>
        <span
          role="button"
          onClick={this.addSubCategory}
          data-ctx="add-button"
        >
          <Icon type="plus" style={{ marginRight: 5, color: '#1890ff' }} />
          <span className={classes.linkStyle}>
            Dodaj podkategorię
          </span>
        </span>

        <Divider type="vertical" />
        <span
          role="button"
          onClick={this.handleEdit}
          data-ctx="edit-button"
        >
          <Icon type="edit" style={{ marginRight: 5, color: '#1890ff' }} />
          <span className={classes.linkStyle}>
            Edytuj kategorię
          </span>
        </span>
        {isEmpty(category.children) &&
        <span>
          <Divider type="vertical" />
          <Icon type="delete" style={{ marginRight: 5, color: '#ED1C24' }} />
          <Popconfirm
            title="Czy na pewno chcesz usunąć tą kategorię?"
            okText="Tak"
            cancelText="Nie"
            placement="left"
            onConfirm={this.handleRemove}
            onCancel={this.stopPropagation}
          >
            <span
              role="button"
              className={classes.linkStyle}
              onClick={this.stopPropagation}
              data-ctx="remove-button"
            >
                Usuń
            </span>
          </Popconfirm>
        </span>
        }
        <span className={classes.statusContainer}>
          <Divider type="vertical" />
          <span className={classes.status}>Status:</span>
          {category.is_visible ?
            <Badge status="success" text="Dostępna" />
            :
            <Badge status="error" text="Ukryta" />
          }
        </span>
      </div>
    )
  }
}

PanelToolbar.propTypes = {
  category: PropTypes.shape({
    is_visible: PropTypes.bool,
    children: PropTypes.array,
    id: PropTypes.number,
  }).isRequired,
  removeCategory: PropTypes.func.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

export default injectSheet(styles)(PanelToolbar);
