import React from 'react';
import { shallow } from 'enzyme';
import { PanelToolbar } from './';

describe('PanelToolbar', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = {
      removeCategory: jest.fn(),
      editCategory: jest.fn(),
      addSubCategory: jest.fn(),
      classes: {},
      category: {
        name: 'Test',
        description: 'test test',
        children: [],
        is_visible: false,
      },
    }
    wrapper = shallow(<PanelToolbar {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should invoke addSubCategory after clicking on addButton', () => {
    const button = wrapper.find('[data-ctx="add-button"]');
    button.simulate('click', { stopPropagation() {} });
    expect(props.addSubCategory).toHaveBeenCalledTimes(1);
  });
  it('should invoke editCategory after clicking on editButton', () => {
    const button = wrapper.find('[data-ctx="edit-button"]');
    button.simulate('click', { stopPropagation() {} });
    expect(props.editCategory).toHaveBeenCalledTimes(1);
  });
  it('should invoke removeCategory after calling onConfirm method in PopConfirm', () => {
    const Popconfirm = wrapper.find('Popconfirm');
    Popconfirm.prop('onConfirm')({ stopPropagation() {} });
    expect(props.removeCategory).toHaveBeenCalledTimes(1);
  });
  it('should change Badge status is is_visbile is truthy', () => {
    expect(wrapper.find('Badge').props().status).toBe('error');
    wrapper.setProps({
      category: {
        ...props.category,
        is_visible: true,
      }
    });
    expect(wrapper.find('Badge').props().status).toBe('success');
  });
  it('should hide remove button if children has values', () => {
    expect(wrapper.find('[data-ctx="remove-button"]')).toHaveLength(1);
    wrapper.setProps({
      category: {
        ...props.category,
        children: [
          { name: 'test' },
        ],
      }
    });
    expect(wrapper.find('[data-ctx="remove-button"]')).toHaveLength(0);
  });
});
