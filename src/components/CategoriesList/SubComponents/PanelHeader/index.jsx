import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip, Icon, Badge } from 'antd';
import injectSheet from 'react-jss';
import { PanelToolbar } from '../index';
import styles from '../../styles';



export const PanelHeader = ({ category, removeCategory, classes, addSubCategory, editCategory }) => (
  <div className={classes.panelContainer}>
    <div className={classes.header}>
      <Badge
        count={category.children.length}
        showZero
        style={{ backgroundColor: category.children.length ? '#25b10f' : '#ED1C24' }}
      />
      <span className={classes.name}>{category.name}</span>
      {category.description && category.description !== ' ' &&
      <Tooltip title={category.description}>
        <Icon type="info-circle" style={{marginLeft: 5, color: 'rgba(0, 0, 0, 0.26)'}} />
      </Tooltip>
      }
    </div>
    <PanelToolbar
      category={category}
      removeCategory={removeCategory}
      editCategory={editCategory}
      addSubCategory={addSubCategory}
    />
  </div>
)

PanelHeader.propTypes = {
  category: PropTypes.shape({
    children: PropTypes.array,
    name: PropTypes.string,
  }).isRequired,
  removeCategory: PropTypes.func.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

export default injectSheet(styles)(PanelHeader);
