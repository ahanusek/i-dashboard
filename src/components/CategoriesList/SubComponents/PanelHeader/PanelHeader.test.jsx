import React from 'react';
import { shallow } from 'enzyme';
import { PanelHeader } from './';

describe('PanelHeader', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = {
      removeCategory: jest.fn(),
      editCategory: jest.fn(),
      addSubCategory: jest.fn(),
      classes: {},
      category: {
        name: 'Test',
        description: 'test test',
        children: [],
      },
    }
    wrapper = shallow(<PanelHeader {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should hide Tooltip and Icon if description is equal null', () => {
    expect(wrapper.find('Tooltip')).toHaveLength(1);
    expect(wrapper.find('Icon')).toHaveLength(1);
    wrapper.setProps({
      category: {
        ...props.category,
        description: null,
      },
    });
    expect(wrapper.find('Tooltip')).toHaveLength(0);
    expect(wrapper.find('Icon')).toHaveLength(0);
  });
  it('should change Badge color if children has values', () => {
    const redColor = '#ED1C24';
    const greenColor = '#25b10f';
    expect(wrapper.find('Badge').props().style.backgroundColor).toBe(redColor)
    wrapper.setProps({
      category: {
        ...props.category,
        children: [
          { name: 'test'}
        ],
      },
    });
    expect(wrapper.find('Badge').props().style.backgroundColor).toBe(greenColor)
  });
});
