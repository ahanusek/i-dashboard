import services from '../../services';
import { ADD_CATEGORY } from "../types";

const addCategory = (data) => ({
  type: ADD_CATEGORY,
  payload: services.addCategory(data),
});

export default addCategory;
