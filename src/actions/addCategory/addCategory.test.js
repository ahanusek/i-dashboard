import addCategory from './';
import { ADD_CATEGORY } from '../types';
import { createMockStore } from '../../helpers';

jest.mock('../../services', () => ({
  addCategory: (data) => Promise.resolve(data)
}))

describe('async addCategory action', () => {
  let mockCategory;
  let store;
  beforeEach(() => {
    store = createMockStore();
    mockCategory = {
      name: 'test',
    };
  });
  it('creates ADD_CATEGORY_FULFILLED when adding category has been done', () => {
    const expectedActions = [
      { type: `${ADD_CATEGORY}_PENDING` },
      { type: `${ADD_CATEGORY}_FULFILLED`, payload: mockCategory },
    ];

    return store.dispatch(addCategory(mockCategory)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates ADD_CATEGORY_REJECTED when adding category has been failed', () => {
    const expectedActions = [
      { type: `${ADD_CATEGORY}_PENDING` },
      { type: `${ADD_CATEGORY}_REJECTED`, payload: new Error(), error: true },
    ];
    return store.dispatch(addCategory(mockCategory)).catch(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
