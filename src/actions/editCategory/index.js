import services from '../../services';
import { EDIT_CATEGORY } from "../types";

const editCategory = (categoryId, data) => ({
  type: EDIT_CATEGORY,
  payload: services.editCategory(categoryId, data),
});

export default editCategory;
