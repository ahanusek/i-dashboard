import editCategory from './';
import { EDIT_CATEGORY } from '../types';
import { createMockStore } from '../../helpers';

jest.mock('../../services', () => ({
  editCategory: (data) => Promise.resolve(data)
}))

describe('async editCategory action', () => {
  let mockCategory;
  let store;
  beforeEach(() => {
    store = createMockStore();
    mockCategory = {
      name: 'test',
    };
  });
  it('creates EDIT_CATEGORY_FULFILLED when updating category has been done', () => {
    const expectedActions = [
      { type: `${EDIT_CATEGORY}_PENDING` },
      { type: `${EDIT_CATEGORY}_FULFILLED`, payload: mockCategory },
    ];

    return store.dispatch(editCategory(mockCategory)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates EDIT_CATEGORY_REJECTED when updating category has been failed', () => {
    const expectedActions = [
      { type: `${EDIT_CATEGORY}_PENDING` },
      { type: `${EDIT_CATEGORY}_REJECTED`, payload: new Error(), error: true },
    ];
    return store.dispatch(editCategory(mockCategory)).catch(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
