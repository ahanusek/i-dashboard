import services from '../../services';
import { FETCH_CATEGORIES } from "../types";

const fetchCategories = () => ({
  type: FETCH_CATEGORIES,
  payload: services.getCategories(),
});

export default fetchCategories;
