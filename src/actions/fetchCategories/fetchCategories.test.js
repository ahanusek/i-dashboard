import fetchCategories from './';
import { FETCH_CATEGORIES } from '../types';
import { createMockStore } from '../../helpers';

const mockList = [
  { id: 1, name: 'test'}
];

jest.mock('../../services', () => ({
  getCategories: () => Promise.resolve(mockList)
}))

describe('async fetchCategories action', () => {
  let store;
  beforeEach(() => {
    store = createMockStore();
  });
  it('creates FETCH_CATEGORIES_FULFILLED when fetching categories has been done', () => {
    const expectedActions = [
      { type: `${FETCH_CATEGORIES}_PENDING` },
      { type: `${FETCH_CATEGORIES}_FULFILLED`, payload: mockList },
    ];

    return store.dispatch(fetchCategories()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates FETCH_CATEGORIES_REJECTED when fetching categories has been failed', () => {
    const expectedActions = [
      { type: `${FETCH_CATEGORIES}_PENDING` },
      { type: `${FETCH_CATEGORIES}_REJECTED`, payload: new Error(), error: true },
    ];
    return store.dispatch(fetchCategories()).catch(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
