import clearCategory from './';
import { CLEAR_CATEGORY } from '../types';
import { createMockStore } from '../../helpers';

describe('clearCategory action', () => {
  const store = createMockStore();
  it('create clearCategory action', () => {
    const expectedAction = [{ type: CLEAR_CATEGORY }];
    store.dispatch(clearCategory());
    expect(store.getActions()).toEqual(expectedAction);
  });
});
