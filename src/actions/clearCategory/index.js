import { CLEAR_CATEGORY } from "../types";

const clearCategory = () => ({
  type: CLEAR_CATEGORY,
});

export default clearCategory;
