import services from '../../services';
import { REMOVE_CATEGORY } from "../types";

const removeCategory = (categoryId) => ({
  type: REMOVE_CATEGORY,
  payload: services.removeCategory(categoryId),
});

export default removeCategory;
