import removeCategory from './';
import { REMOVE_CATEGORY } from '../types';
import { createMockStore } from '../../helpers';

jest.mock('../../services', () => ({
  removeCategory: () => Promise.resolve()
}))

describe('async removeCategory action', () => {
  const categoryId = 102;
  let store;
  beforeEach(() => {
    store = createMockStore();
  });
  it('creates REMOVE_CATEGORY_FULFILLED when removing category has been done', () => {
    const expectedActions = [
      { type: `${REMOVE_CATEGORY}_PENDING` },
      { type: `${REMOVE_CATEGORY}_FULFILLED`},
    ];

    return store.dispatch(removeCategory(categoryId)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it('creates REMOVE_CATEGORY_REJECTED when removing category has been failed', () => {
    const expectedActions = [
      { type: `${REMOVE_CATEGORY}_PENDING` },
      { type: `${REMOVE_CATEGORY}_REJECTED`, payload: new Error(), error: true },
    ];
    return store.dispatch(removeCategory(categoryId)).catch(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
