import get from 'lodash/get';
import initialState from './initialState';
import { FETCH_CATEGORIES } from "../../actions/types";
import reqStatus from '../../enums/requestStatus';
import { removeRootCategory, createCategoriesTree } from "../../helpers/index";

const categoriesReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case `${FETCH_CATEGORIES}_PENDING`: {
      return {
        ...initialState,
        error: null,
        status: reqStatus.PENDING
      }
    }
    case `${FETCH_CATEGORIES}_FULFILLED`: {
      const { payload } = action;
      return {
        ...initialState,
        status: reqStatus.FULFILLED,
        data: [
          ...createCategoriesTree(removeRootCategory(get(payload, 'data.data.categories', [])))
        ],
      }
    }
    case `${FETCH_CATEGORIES}_REJECTED`: {
      const { payload } = action;
      return {
        ...initialState,
        status: reqStatus.REJECTED,
        error: get(payload, 'response.status', 404),
      }
    }
    default:
      return state;
  }
};

export default categoriesReducer;
