import initialState from './initialState';
import { FETCH_CATEGORIES } from "../../actions/types";
import request from '../../enums/requestStatus';
import { createCategoriesTree } from "../../helpers/index";
import categoriesReducer from './';

describe('categoriesReducer', () => {
  it('should have initial state', () => {
    expect(categoriesReducer()).toEqual(initialState);
  })
  it('should handle FETCH_CATEGORIES_PENDING', () => {
    const action = {
      type: `${FETCH_CATEGORIES}_PENDING`,
    };
    const expectedState = {
      ...initialState,
      status: request.PENDING,
    };
    expect(categoriesReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle FETCH_CATEGORIES_FULFILLED', () => {
    const mockPayload = {
      data: {
        data: {
          categories: [
            {id: 1, name: 'test'},
            {id: 2, name: 'test2'},
          ]
        }
      },
    };
    const action = {
      type: `${FETCH_CATEGORIES}_FULFILLED`,
      payload: mockPayload,
    };
    const expectedState = {
      ...initialState,
      data: createCategoriesTree(action.payload.data.data.categories),
      status: request.FULFILLED,
    };
    expect(categoriesReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle FETCH_CATEGORIES_REJECTED', () => {
    const mockPayload = {
      response: {
        status: 500,
      },
      errorMessage: 'Bad url',
    };
    const action = {
      type: `${FETCH_CATEGORIES}_REJECTED`,
      payload: mockPayload,
    };
    const expectedState = {
      ...initialState,
      error: action.payload.response.status,
      status: request.REJECTED,
    };
    expect(categoriesReducer(initialState, action)).toEqual(expectedState);
  });
})
