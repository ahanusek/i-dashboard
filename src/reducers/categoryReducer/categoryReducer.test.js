import initialState from './initialState';
import { ADD_CATEGORY, EDIT_CATEGORY, REMOVE_CATEGORY, CLEAR_CATEGORY } from "../../actions/types";
import request from '../../enums/requestStatus';
import categoryReducer from './';

describe('categoryReducer', () => {
  it('should have initial state', () => {
    expect(categoryReducer()).toEqual(initialState);
  })
  it('should handle CLEAR_CATEGORY', () => {
    const action = {
      type: CLEAR_CATEGORY,
    };
    const state = {
      data: ['test'],
      error: true,
      status: request.ADDING,
    };
    expect(categoryReducer(state, action)).toEqual(initialState);
  })
  it('should handle ADD_CATEGORY_PENDING', () => {
    const action = {
      type: `${ADD_CATEGORY}_PENDING`,
    };
    const expectedState = {
      ...initialState,
      status: request.ADDING,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle ADD_CATEGORY_FULFILLED', () => {
    const action = {
      type: `${ADD_CATEGORY}_FULFILLED`,
    };
    const expectedState = {
      ...initialState,
      status: request.FULFILLED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle ADD_CATEGORY_REJECTED', () => {
    const mockPayload = {
      response: {
        status: 500,
      },
    };
    const action = {
      type: `${ADD_CATEGORY}_REJECTED`,
      payload: mockPayload,
    };
    const expectedState = {
      ...initialState,
      error: action.payload.response.status,
      status: request.REJECTED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle EDIT_CATEGORY_PENDING', () => {
    const action = {
      type: `${EDIT_CATEGORY}_PENDING`,
    };
    const expectedState = {
      ...initialState,
      status: request.UPDATING,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle EDIT_CATEGORY_FULFILLED', () => {
    const action = {
      type: `${EDIT_CATEGORY}_FULFILLED`,
    };
    const expectedState = {
      ...initialState,
      status: request.FULFILLED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle EDIT_CATEGORY_REJECTED', () => {
    const mockPayload = {
      response: {
        status: 500,
      },
    };
    const action = {
      type: `${EDIT_CATEGORY}_REJECTED`,
      payload: mockPayload,
    };
    const expectedState = {
      ...initialState,
      error: action.payload.response.status,
      status: request.REJECTED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle REMOVE_CATEGORY_PENDING', () => {
    const action = {
      type: `${REMOVE_CATEGORY}_PENDING`,
    };
    const expectedState = {
      ...initialState,
      status: request.REMOVING,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle REMOVE_CATEGORY_FULFILLED', () => {
    const action = {
      type: `${REMOVE_CATEGORY}_FULFILLED`,
    };
    const expectedState = {
      ...initialState,
      status: request.FULFILLED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
  it('should handle REMOVE_CATEGORY_REJECTED', () => {
    const mockPayload = {
      response: {
        status: 500,
      },
    };
    const action = {
      type: `${REMOVE_CATEGORY}_REJECTED`,
      payload: mockPayload,
    };
    const expectedState = {
      ...initialState,
      error: action.payload.response.status,
      status: request.REJECTED,
    };
    expect(categoryReducer(initialState, action)).toEqual(expectedState);
  });
})
