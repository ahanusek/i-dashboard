import get from 'lodash/get';
import initialState from './initialState';
import { ADD_CATEGORY, EDIT_CATEGORY, REMOVE_CATEGORY, CLEAR_CATEGORY } from "../../actions/types";
import reqStatus from '../../enums/requestStatus';

const categoryReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case CLEAR_CATEGORY: {
      return {
        ...initialState,
      }
    }
    case `${ADD_CATEGORY}_PENDING`: {
      return {
        ...initialState,
        error: null,
        status: reqStatus.ADDING,
      }
    }
    case `${ADD_CATEGORY}_FULFILLED`: {
      return {
        ...initialState,
        status: reqStatus.FULFILLED,
      }
    }
    case `${ADD_CATEGORY}_REJECTED`: {
      const { payload } = action;
      return {
        ...initialState,
        status: reqStatus.REJECTED,
        error: get(payload, 'response.status', 404),
      }
    }
    case `${REMOVE_CATEGORY}_PENDING`: {
      return {
        ...initialState,
        error: null,
        status: reqStatus.REMOVING,
      }
    }
    case `${REMOVE_CATEGORY}_FULFILLED`: {
      return {
        ...initialState,
        status: reqStatus.FULFILLED,
      }
    }
    case `${REMOVE_CATEGORY}_REJECTED`: {
      const { payload } = action;
      return {
        ...initialState,
        status: reqStatus.REJECTED,
        error: get(payload, 'response.status', 404),
      }
    }
    case `${EDIT_CATEGORY}_PENDING`: {
      return {
        ...initialState,
        error: null,
        status: reqStatus.UPDATING,
      }
    }
    case `${EDIT_CATEGORY}_FULFILLED`: {
      return {
        ...initialState,
        status: reqStatus.FULFILLED,
      }
    }
    case `${EDIT_CATEGORY}_REJECTED`: {
      const { payload } = action;
      return {
        ...initialState,
        status: reqStatus.REJECTED,
        error: get(payload, 'response.status', 404),
      }
    }
    default:
      return state;
  }
};

export default categoryReducer;
