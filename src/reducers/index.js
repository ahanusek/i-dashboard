// Set up your root reducer here...
import { combineReducers } from 'redux';
import categories from './categoriesReducer';
import category from './categoryReducer';

const rootReducer = combineReducers({
  categories,
  category,
});

export default rootReducer;
