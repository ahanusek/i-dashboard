import createCategoryPostObject from './';

describe('createCategoryPostObject helper', () => {
  it('should proper formatted posting object', () => {
    const model = {
      name: 'Test',
      is_visible: true,
    };
    const expected = {
      category: {
        parent_id: 1,
        is_visible: model.is_visible,
        name: model.name,
        description: ' ', //it's workaround for bug in API
        picture_filename: '',
      }
    };
    expect(createCategoryPostObject(model)).toEqual(expected);
  });
});
