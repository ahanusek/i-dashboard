
const createCategoryPostObject = (model, parentId) => {
  const parent_id = parentId || model.parent_id;
  return {
    category: {
      parent_id: parent_id || 1,
      is_visible: model.is_visible,
      name: model.name,
      description: model.description || ' ', //it's workaround for bug in API
      picture_filename: '',
    }
  }
}


export default createCategoryPostObject;
