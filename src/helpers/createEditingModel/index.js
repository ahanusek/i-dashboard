import isEmpty from 'lodash/isEmpty';

const createEditingModel = (model) => {
  if (isEmpty(model)) return null;
  return {
    name: model.name || '',
    description: model.description || '',
    is_visible: model.is_visible || false,
  }
}

export default createEditingModel;
