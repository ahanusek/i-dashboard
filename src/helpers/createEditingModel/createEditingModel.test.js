import createEditingModel from './';

describe('createEditingModel helper', () => {
  it('should return null if model is empty', () => {
    expect(createEditingModel({})).toBe(null);
  });
  it('should return proper formatted model', () => {
    const model = {
      name: 'Test',
      is_visible: true,
      description: 'test test',
      id: 3,
      parent_id: 10,
      symbol: 'ying',
    };
    const expected = {
      name: 'Test',
      is_visible: true,
      description: 'test test',
    };
    expect(createEditingModel(model)).toEqual(expected);
  });
  it('should return model with default values', () => {
    const model = {
      id: 3,
      parent_id: 10,
      symbol: 'ying',
    };
    const expected = {
      name: '',
      is_visible: false,
      description: '',
    };
    expect(createEditingModel(model)).toEqual(expected);
  });
});
