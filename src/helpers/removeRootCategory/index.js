
const removeRootCategory = (categories) => categories
  .filter(category => category.id !== 1 && category.name !== 'ROOT');


export default removeRootCategory;
