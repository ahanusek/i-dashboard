import removeRootCategory from './';

describe('removeRootCategory helper', () => {
  it('should remove object if has id equal 1 and name ROOT', () => {
    const mockCategories = [
      {
        name: 'Test',
        id: 2,
      },
      {
        name: 'ROOT',
        id: 1,
      }
    ];
    const expected = [
      {
        name: 'Test',
        id: 2,
      },
    ]
    expect(removeRootCategory(mockCategories)).toEqual(expected);
  })
})
