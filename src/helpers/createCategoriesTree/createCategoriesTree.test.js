import createCategoriesTree from './';

describe('createCategoriesTree helper', () => {
  it('should return proper categories tree', () => {
    const categories = [
      {
        id: 16,
        parent_id: 1,
        name: 'test'
      },
      {
        id: 20,
        parent_id: 1,
        name: 'testTest',
      },
      {
        id: 17,
        parent_id: 1,
        name: 'test2'
      },
      {
        id: 24,
        parent_id: 17,
        name: 'test3',
      },
      {
        id: 40,
        parent_id: 16,
        name: 'test4',
      },
      {
        id: 99,
        parent_id: 40,
        name: 'test7'
      },
    ];
    const expected = [
      {
        id: 16,
        parent_id: 1,
        name: 'test',
        children: [
          {
            id: 40,
            parent_id: 16,
            name: 'test4',
            children: [
              {
                id: 99,
                parent_id: 40,
                name: 'test7',
                children: [],
              },
            ]
          },
        ]
      },
      {
        id: 20,
        parent_id: 1,
        name: 'testTest',
        children: [],
      },
      {
        id: 17,
        parent_id: 1,
        name: 'test2',
        children: [
          {
            id: 24,
            parent_id: 17,
            name: 'test3',
            children: [],
          },
        ]
      },
    ];
    expect(createCategoriesTree(categories)).toEqual(expected);
  });
  it('should return empty array if categories array is empty', () => {
    expect(createCategoriesTree([])).toEqual([]);
  })
});
