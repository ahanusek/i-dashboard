import _ from 'lodash';

const createCategoriesTree = (categories) => {
  const categoriesTree = [...categories]
  _(categoriesTree).forEach(f=> {
      f.children = _(categoriesTree)
      .filter(g=>g.parent_id === f.id).value();

  });
  return _(categoriesTree).filter(f=>f.parent_id === 1).value();
};

export default createCategoriesTree;
