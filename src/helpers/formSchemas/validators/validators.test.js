import maxLengthValidator from './index';

describe('validator - maxLengthValidator', () => {
  let max;
  beforeEach(() => {
    max = maxLengthValidator(10);
  });
  it('should return true if does not exceeds max value of 10', () => {
    expect(max.validator('Test')).toBe(true);
  });
  it('should return false if exceeds max value of 10', () => {
    expect(max.validator('This test check if the value is higher' +
      'than 10 and if so returns false')).toBe(false);
  });
});
