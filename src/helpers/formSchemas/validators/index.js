
const maxLengthValidator = (maxValue) => ({
  validator(value){
    if (value && value.length > maxValue) {
      return false;
    }
    return true;
  },
  errorMessage: `Podana wartość nie może być większa niż ${maxValue} znaków`
});

export default maxLengthValidator;
