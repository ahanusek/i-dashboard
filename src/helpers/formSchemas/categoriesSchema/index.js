import Schema from 'form-schema-validation';
import maxLengthValidator from "../validators";

const ErrorMessages = {
  notDefinedKey(key) { return `Key '${key}' is not defined in schema`; },
  modelIsUndefined() { return 'Validated model is undefined'; },
  validateRequired() { return `Pole jest wymagane`; },
  validateString() { return `Niepoprawny format danych`; },
  validateNumber() { return `Podane dane nie są liczbą`; },
  validateObject(key) { return `Field '${key}' is not a Object`; },
  validateArray(key) { return `Field '${key}' is not a Array`; },
  validateBoolean(key) { return `Field '${key}' is not a Boolean`; },
  validateDate(key) { return `Field '${key}' is not a Date`; },
};


const categoriesSchema = new Schema({
  name: {
    type: String,
    required: true,
    validators: [maxLengthValidator(50)]
  },
  description: {
    type: Schema.optionalType(String),
    validators: [maxLengthValidator(150)]
  },
  is_visible: {
    type: Boolean,
    defaultValue: true,
  },
}, ErrorMessages, false);

export default categoriesSchema;
