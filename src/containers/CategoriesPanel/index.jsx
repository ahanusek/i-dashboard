import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Layout, Spin, Alert, message  } from 'antd';
import omit from 'lodash/omit';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import injectSheet from 'react-jss';
import styles from './styles';
import reqStatus from '../../enums/requestStatus';
import { fetchCategories, addCategory, clearCategory, removeCategory, editCategory } from "../../actions/index";
import { createCategoryPostObject, createEditingModel } from "../../helpers/index";
import { CategoriesForm, CategoriesList } from "../../components/index";

const { Content } = Layout;


export class CategoriesPanel extends Component {
  state = { showModal: false, model: null, parent_id: null };
  componentDidMount() {
    this.props.fetchCategories()
  }
  onSubmit = (formData) => {
    const { parent_id, model } = this.state;
    if (!isEmpty(model)) {
      const editedCategory = {
        ...model,
        ...formData,
      }
      this.props.editCategory(model.id, createCategoryPostObject(editedCategory))
        .then(() => {
          message.success('Poprawnie edytowano kategorię');
          this.closeModal()
          this.props.fetchCategories();
        })
    } else {
      this.props.addCategory(createCategoryPostObject(formData, parent_id))
        .then(() => {
          message.success('Dodano nową kategorię');
          this.closeModal()
          this.props.fetchCategories();
        })
    }

  }
  editCategory = (model) => {
    this.setState({
      showModal: true,
      model: {
        ...omit(model, ['children', 'source_id', 'symbol'])
        },
      })
  }
  addSubCategory = (parentId) => {
    this.setState({
      showModal: true,
      parent_id: parentId,
    })
  }
  removeCategory = (categoryId) => {
    message.loading('Usuwanie w trakcie...', 0)
    this.props.removeCategory(categoryId)
      .then(() => {
        message.destroy()
        message.warning('Usunięto poprawnie kategorię');
        this.props.fetchCategories()
      })
      .catch(() => {
        message.destroy()
        if (this.props.categoryError === 500) {
          message.error('Nie masz uprawnień do usunięcia tej kategorii. Skontaktuj się z administratorem.');
        } else {
          message.error('Nie udało się poprawnie usunąć kategorii. Spróbuj ponownie.');
        }
        this.props.clearCategory();
      })
  }
  toggleModal = () => {
    this.setState((prevState) => ({
      showModal: !prevState.showModal,
    }));
  };
  closeModal = () => {
    this.setState({
      showModal: false,
      model: null,
      parent_id: null,
    }, () => {
      this.props.clearCategory();
    })
  }
  renderStatus = () => {
    const { categoriesStatus } = this.props;
    if (categoriesStatus === reqStatus.PENDING) return <Spin />;
    if (categoriesStatus === reqStatus.REJECTED) {
      return (
        <Alert
          message="Nie udało się pobrać listy kategorii"
          description="Odśwież stronę i spróbuj ponownie"
          type="error"
          showIcon
        />
      )
    }
  }
  render() {
    const { showModal, model } = this.state;
    const { classes, categories, categoriesStatus, categoryStatus, categoryError } = this.props;
    return (
      <Fragment>
        <Content className={classes.container}>
          <Card
            title="Zarządzaj kategoriami"
            extra={
              <Button
                type="primary"
                icon="plus"
                onClick={this.toggleModal}
              >
                Dodaj kategorię
              </Button>
            }
          >
            <div className={classes.content}>
              {categoriesStatus !== reqStatus.FULFILLED ?
                this.renderStatus()
                :
                <CategoriesList
                  categories={categories}
                  removeCategory={this.removeCategory}
                  editCategory={this.editCategory}
                  addSubCategory={this.addSubCategory}
                />
              }
            </div>
          </Card>
        </Content>
        <CategoriesForm
          title={model ? "Edytuj kategorię" : "Dodaj nową kategorię"}
          open={showModal}
          handleClose={this.closeModal}
          model={createEditingModel(model)}
          onSubmit={this.onSubmit}
          loading={categoryStatus === reqStatus.ADDING || categoryStatus === reqStatus.UPDATING}
          errorObject={categoryError}
        />
      </Fragment>
    )
  }
}

CategoriesPanel.defaultProps = {
  categories: [],
  categoryError: null,
  categoryStatus: null,
  categoriesStatus: null,
};

CategoriesPanel.propTypes = {
  fetchCategories: PropTypes.func.isRequired,
  addCategory: PropTypes.func.isRequired,
  removeCategory: PropTypes.func.isRequired,
  clearCategory: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired,
  categoryError: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.string]),
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  categories: PropTypes.arrayOf(PropTypes.object),
  categoryStatus: PropTypes.oneOf([
    reqStatus.UPDATING,
    reqStatus.REMOVING,
    reqStatus.ADDING,
    reqStatus.FULFILLED,
    reqStatus.REJECTED,
  ]),
  categoriesStatus: PropTypes.oneOf([
    reqStatus.PENDING,
    reqStatus.FULFILLED,
    reqStatus.REJECTED,
  ]),
};

function mapStateToProps(state) {
  return {
    categories: state.categories.data,
    categoriesStatus: state.categories.status,
    categoryStatus: state.category.status,
    categoryError: state.category.error,
  }
}

export default connect(mapStateToProps,
  { fetchCategories,
    addCategory,
    clearCategory,
    removeCategory,
    editCategory,
  })(injectSheet(styles)(CategoriesPanel));
