const styles = () => ({
  content: {
    display: 'flex',
    justifyContent: 'center',
    minHeight: 150,
    alignItems: 'center',
    padding: '20px 0',
  }
});

export default styles;
