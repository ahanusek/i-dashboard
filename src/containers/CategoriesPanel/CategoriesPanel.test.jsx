import React from 'react';
import { ThemeProvider } from 'react-jss';
import { shallow, mount } from 'enzyme';
import reqStatus from '../../enums/requestStatus';
import { CategoriesList } from '../../components';
import { CategoriesPanel } from './';

describe('CategoriesPanel', () => {
  let wrapper;
  let props;
  let spyAddCategory = jest.fn();
  let spyEditCategory = jest.fn();
  let spyRemoveCategory = jest.fn();
  let mockTheme;
  beforeEach(() => {
    mockTheme = {
      colors: {
        linkStyle: '#fff',
        lightBlue: '#f0f',
      }
    }
    props = {
      removeCategory: () => Promise.resolve(spyRemoveCategory()),
      editCategory: () => Promise.resolve(spyEditCategory()),
      addCategory: () => Promise.resolve(spyAddCategory()),
      fetchCategories: jest.fn(),
      clearCategory: jest.fn(),
      categoriesStatus: reqStatus.FULFILLED,
      categories: [],
      classes: {},
    }
    wrapper = shallow(<CategoriesPanel {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should invoke fetchCategories on componentDidMount', () => {
    wrapper = mount(<ThemeProvider theme={mockTheme}><CategoriesPanel {...props} /></ThemeProvider>)
    expect(props.fetchCategories).toHaveBeenCalled();
  })
  it('should render Spin and hide CategoriesList if categoriesStatus is equal PENDING', () => {
    expect(wrapper.find(CategoriesList)).toHaveLength(1);
    expect(wrapper.find('Spin')).toHaveLength(0);
    wrapper.setProps({
      categoriesStatus: reqStatus.PENDING,
    });
    expect(wrapper.find(CategoriesList)).toHaveLength(0);
    expect(wrapper.find('Spin')).toHaveLength(1);
  })
  it('should render error Alert and hide CategoriesList if categoriesStatus is equal REJECTED', () => {
    expect(wrapper.find(CategoriesList)).toHaveLength(1);
    expect(wrapper.find('Alert')).toHaveLength(0);
    wrapper.setProps({
      categoriesStatus: reqStatus.REJECTED,
    });
    expect(wrapper.find(CategoriesList)).toHaveLength(0);
    expect(wrapper.find('Alert')).toHaveLength(1);
  })
  it('should invoke clearCategory after calling handleClose in Form', () => {
    const Form = wrapper.find('Jss(ModalForm)');
    Form.prop('handleClose')();
    expect(props.clearCategory).toHaveBeenCalledTimes(1);
  })
  it('should invoke addCategory after calling onSubmit in Form when model is empty', () => {
    const Form = wrapper.find('Jss(ModalForm)');
    Form.prop('onSubmit')({ name: 'test' });
    expect(spyAddCategory).toHaveBeenCalledTimes(1);
  })
  it('should invoke editCategory after calling onSubmit in Form when model isn\'t empty', () => {
    wrapper.setState({
      model: {
        parent_id: 2,
        name: 'test2',
      }
    })
    const Form = wrapper.find('Jss(ModalForm)');
    Form.prop('onSubmit')({ name: 'test' });
    expect(spyEditCategory).toHaveBeenCalledTimes(1);
  })
  it('should invoke removeCategory after calling removeCategory in CategoriesList', () => {
    const List = wrapper.find('Jss(CategoriesList)');
    List.prop('removeCategory')(2);
    expect(spyRemoveCategory).toHaveBeenCalledTimes(1);
  })
});
