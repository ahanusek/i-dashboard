import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Icon, Menu } from 'antd';
import { Switch, Route } from 'react-router-dom';
import injectSheet from 'react-jss';
import { CategoriesPanel } from "../index";
import styles from './styles';

const { Header, Sider } = Layout;

export const Dashboard = ({ classes, match }) => (
  <Layout>
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      className={classes.sider}
    >
      <div className={classes.logoContainer}>
        <span className={classes.brand}>i-dashboard</span>
        <Icon
          type="appstore"
          style={{ fontSize: 20, color: '#00aeef' }}
        />
      </div>
      <Menu
        mode="inline"
        selectedKeys={['categories']}
      >
        <Menu.Item key="categories">
          <Icon type="table" />
          <span className="nav-text">Panel kategorii</span>
        </Menu.Item>
      </Menu>
    </Sider>
    <Layout>
      <Header className={classes.header} />
      <div className={classes.contentContainer}>
        <Switch>
          <Route exact path={match.path} component={CategoriesPanel} />
        </Switch>
      </div>
    </Layout>
  </Layout>
)

Dashboard.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  match: PropTypes.shape({
    path: PropTypes.string,
  }).isRequired,
}

export default (injectSheet(styles)(Dashboard));
