import React from 'react';
import { shallow } from 'enzyme';
import { Dashboard } from './';

describe('Dashboard', () => {
  let wrapper;
  let props;
  beforeEach(() => {
    props = {
      classes: {},
      match: {
        path: 'test',
      }
    }
    wrapper = shallow(<Dashboard {...props} />)
  })
  it('should exists', () => {
    expect(wrapper.exists()).toBe(true);
  });
  it('should render Menu', () => {
    expect(wrapper.find('Menu')).toHaveLength(1);
  });
});
