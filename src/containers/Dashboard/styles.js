const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.colors.backgroundColor,
      margin: 0,
      padding: 0,
    },
    '.ant-input-group.ant-input-group-compact': {
      marginTop: 5,
    },
    '.hidden-field': {
      display: 'none',
    },
  },
  sider: {
    backgroundColor: theme.colors.secondary,
    minHeight: '100vh',
    borderRight: theme.borders.containerBorder,
    '& .ant-menu-inline': {
      borderRight: 'none',
    },
    '& .ant-menu-item': {
      marginTop: 0,
    },
  },
  contentContainer: {
    margin: 30,
    '& .ant-table-pagination.ant-pagination': {
      marginBottom: 0,
    },
    '& form textarea.ant-input': {
      marginTop: 10,
    },
    '& .ant-card-head-wrapper': {
      alignItems: 'center',
    },
    '& .ant-input-group.ant-input-group-compact > *': {
      verticalAlign: 'inherit',
      marginTop: 3,
    },
  },
  spinnerContainer: {
    margin: '50px auto',
    textAlign: 'center',
  },
  logoContainer: {
    height: 70,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: theme.borders.containerBorder,
  },
  header: {
    height: 70,
    backgroundColor: theme.colors.secondary,
    borderBottom: theme.borders.containerBorder,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  brand: {
    fontWeight: 500,
    fontSize: 16,
    marginRight: 10,
  },
  root: {
    marginTop: theme.spacing.unit * 3,
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
  menuItem: {
    minWidth: 250,
  },
  menuIcon: {
    marginLeft: 10,
  },
});

export default styles;
