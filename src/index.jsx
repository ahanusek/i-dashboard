import React from 'react';
import { render } from 'react-dom';
import { ThemeProvider } from 'react-jss';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter } from 'react-router-dom';
import 'antd/dist/antd.css';
import configureStore, { history } from './store/configureStore';
import Root from './Root';
import theme from './styles/theme';

require('./favicon.ico');
// Tell webpack to load favicon.ico
const store = configureStore();


render(
  <BrowserRouter>
    <AppContainer>
      <ThemeProvider theme={theme}>
        <Root store={store} history={history} />
      </ThemeProvider>
    </AppContainer>
  </BrowserRouter>,
  document.getElementById('app'),
);

if (module.hot) {
  module.hot.accept('./Root', () => {
    const NewRoot = require('./Root').default;
    render(
      <BrowserRouter>
        <AppContainer>
          <ThemeProvider theme={theme}>
            <NewRoot store={store} history={history} />
          </ThemeProvider>
        </AppContainer>
      </BrowserRouter>,
      document.getElementById('app'),
    );
  });
}
