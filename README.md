# i-dashboard

Prosty dashboard do zarządzania kategoriami z wykorzystaniem ReactJS, Redux i biblioteki komponentów UI Ant-Design.

#####[LIVE DEMO](http://ahanusek.pl/)

## Get Started

1. **Install dependencies**. `npm install`
2. **Run the dev version**. `npm start -s`
3. **Run the prod version**. `npm run build`
3. **Run the test coverage**. `npm run test:cover`

