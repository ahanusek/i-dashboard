# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.4.0"></a>
# [0.4.0](https://bitbucket.org/ahanusek/i-dashboard/compare/v0.3.0...v0.4.0) (2018-03-07)


### Features

* Added unit tests for components ([748ee7d](https://bitbucket.org/ahanusek/i-dashboard/commits/748ee7d))



<a name="0.3.0"></a>
# [0.3.0](https://bitbucket.org/ahanusek/i-dashboard/compare/v0.2.0...v0.3.0) (2018-03-07)


### Features

* Added actions unit tests ([6ee3e32](https://bitbucket.org/ahanusek/i-dashboard/commits/6ee3e32))



<a name="0.2.0"></a>
# 0.2.0 (2018-03-07)


### Features

* Add main features ([f52beb7](https://bitbucket.org/ahanusek/i-dashboard/commits/f52beb7))


<a name="0.1.0"></a>
# 0.1.0 (2018-03-06)


### Features

* Initial commit
